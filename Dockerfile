# pull official base image
FROM python:3.9.6-alpine

# set work directory
WORKDIR /accuknox_assignment

# set environment variables
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

# install psycopg2 dependencies
RUN apk update \
    && apk add postgresql-dev gcc python3-dev musl-dev

# install dependencies
RUN pip install --upgrade pip
COPY ./requirement.txt .
RUN pip install -r requirement.txt

# copy project
COPY . .

