from django.contrib.auth.models import User
from django.db import models

# Create your models here.


class UserActivities(models.Model):
    objects = None  # type: None
    sender = models.CharField(max_length=128)
    receiver = models.CharField(max_length=128)
    request_sent_date = models.DateTimeField(auto_now_add=True)
    request_status = models.CharField(max_length=128)
    status_change_date = models.DateTimeField(null=True)
    last_updated_by = models.CharField(max_length=128)
    last_updated = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = "user_activity"


class Posts(models.Model):
    object = None
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    title = models.CharField(max_length=255)
    description = models.TextField()
    created_on = models.DateTimeField(auto_now_add=True)
    is_deleted = models.BooleanField(default=False)
    deleted_on = models.DateTimeField(null=True)
    updated_at = models.DateTimeField(auto_now=True)

