import json

from django.db.models import Q
from django.http import HttpResponse
from django.shortcuts import render
from rest_framework import status
from rest_framework.authtoken.admin import User
from rest_framework.decorators import api_view
from rest_framework.response import Response


# Create your views here.

@api_view(["GET"])
def search_user(request):
    """
    :For User Sign Up
    :param request:username ,password
    :return:token key
    :url auth/login/
    """
    empty_msg = "No data found"
    try:
        input_string = request.GET.get('input_string')
        exact_data = list(User.objects.filter(email=input_string).values(
            'username', 'first_name', 'last_name', 'email'
        ))
        if exact_data:
            return Response(exact_data, status=status.HTTP_200_OK)

        search_data = User.objects.filter(first_name__icontains=input_string).values_list(
            'first_name', flat=True).order_by('first_name').distinct()
        print(search_data, '----search data')
        if search_data:
            return Response(search_data, status=status.HTTP_200_OK)
        return Response({'msg': empty_msg}, status=status.HTTP_200_OK)
    except Exception as e:
        print("Error from login method: %s" % e)
        return Response({"error": str(e)}, status=status.HTTP_400_BAD_REQUEST)