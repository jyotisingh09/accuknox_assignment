from django.contrib.auth.models import User
from django.db import models


class ExtendedUser(models.Model):
    objects = None  # type: None
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    country = models.CharField(max_length=128, null=True, blank=True)

    class Meta:
        db_table = "extender_user"
