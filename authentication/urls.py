"""
urls docstring
"""
from django.urls import path

from authentication import views
from authentication.views import UserRecordsView

urlpatterns = [
    # authentication
    path('login/', views.login, name='login'),
    path('signup/', views.signup, name='user sign up'),
    path('user_details/', UserRecordsView.as_view(), name='fetching all users'),

]

