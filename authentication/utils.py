import re
from django.contrib.auth import authenticate
from rest_framework.authtoken.models import Token


def validate_email_pattern(s):
    pat = r'\b[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Z|a-z]{2,7}\b'
    if re.match(pat,s):
        return True
    else:
        return False


def validate_user(email, request):
    """

    """
    user_data = dict()
    try:
        user = authenticate(request, username=email, password=request.data['password'])
        if Token.objects.filter(user=user).exists():
            token_obj = Token.objects.get(user=user)
            token_obj.delete()

        token, _ = Token.objects.get_or_create(user=user)

        user_data.update({
            "id": user.id,
            "token": token.key,
            "email": user.email,
        })
        return True, user_data

    except Exception as e:
        print(e, "error in validate_user")
        return False, dict()
