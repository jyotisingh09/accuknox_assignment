import json
import datetime

from django.db.models import Q
from django.contrib.auth.models import User
from rest_framework.views import APIView
from rest_framework import (status)
from rest_framework.decorators import (api_view, permission_classes)
from rest_framework.response import Response
from rest_framework.pagination import PageNumberPagination
from rest_framework.generics import ListAPIView
from rest_framework.filters import SearchFilter, OrderingFilter
from authentication.serializer import UserSerializer
from authentication.utils import validate_email_pattern, validate_user


# Create your views here.

@api_view(["POST"])
def login(request):
    """
    :For login
    :param request:username ,password
    :return:token key
    :url auth/login/
    """
    error_msg = "username or password is incorrect"
    not_exist_msg = "This email does not exist"
    enter_email_msg = "please enter valid email address"
    no_data = "Please send email and password"
    try:
        if "email" in request.data:
            email = request.data.get("email").lower().strip()
            correct_email_format = validate_email_pattern(email)
            if not correct_email_format:
                return Response({"message": enter_email_msg}, status=status.HTTP_401_UNAUTHORIZED)
            user_exists = User.objects.filter(Q(email=email)).exists()
            if not user_exists:
                return Response({"message": not_exist_msg}, status=status.HTTP_401_UNAUTHORIZED)
            msg, data = validate_user(email, request)
            if not msg:
                return Response({"message": error_msg}, status=status.HTTP_401_UNAUTHORIZED)
            return Response({"message": data}, status=status.HTTP_200_OK)
        else:
            return Response({"message": no_data}, status=status.HTTP_400_BAD_REQUEST)

    except Exception as e:
        print("Error from login method: %s" % e)
        return Response({"error": str(e)}, status=status.HTTP_401_UNAUTHORIZED)


@api_view(["POST"])
def signup(request):
    """
    :For User Sign Up
    :param request:username ,password
    :return:token key
    :url auth/login/
    """
    try:
        print(request.data)
        email = request.data['email'].lower().strip()
        request.data['username'] = email
        print(request.data, '----after')
        serializer = UserSerializer(data=request.data)
        if serializer.is_valid(raise_exception=ValueError):
            print('hi')
            flag = User.objects.filter(Q(email=email)).exists()  # check if email exists
            print(flag)
            if not flag:
                print(request.data)
                serializer.create(validated_data=request.data)

                return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response({"msg": "User with this email already exists"}, status=status.HTTP_302_FOUND)

    except Exception as e:
        print("Error from login method: %s" % e)
        return Response({"error": str(e)}, status=status.HTTP_400_BAD_REQUEST)


class UserRecordsView(ListAPIView):
    # permission_classes = (IsAuthenticatedUser, )
    queryset = User.objects.all()
    serializer_class = UserSerializer
    pagination_class = PageNumberPagination
    filter_backends = (SearchFilter, OrderingFilter)
    search_fields = ('email', 'first_name', 'last_name')
